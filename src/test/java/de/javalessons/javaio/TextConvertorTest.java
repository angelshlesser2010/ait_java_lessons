package de.javalessons.javaio;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

class TextConvertorTest {

    @Test
    void testConvertWordToLowerCase() throws IOException {
        TextConvertor textConvertor = new TextConvertor();
        StringReader stringReader = new StringReader("YESTERDAY");
        StringWriter stringWriter = new StringWriter();
        String result = textConvertor.convertToLowerCase(stringReader, stringWriter);
        Assertions.assertEquals("yesterday", result);
    }
}