package de.javalessons.homework51;

import de.javalessons.exceptions.CalculatorException;
import nl.altindag.log.LogCaptor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import static org.junit.jupiter.api.Assertions.*;

class FileTextWorkerTest {

    private LogCaptor logCaptor = LogCaptor.forClass(FileTextWorker.class);


    private FileTextWorker fileTextWorker;

    @BeforeEach
    private void setUp(){
        fileTextWorker = new FileTextWorker();
    }

    @Test
    void testWriteReversedTextToFileSuccess() throws IOException {
        logCaptor.setLogLevelToInfo();
        String testToSeverse = "Lesson";
        StringReader stringReader = new StringReader(testToSeverse);
        StringWriter stringWriter = new StringWriter();
        String expectedInfoMessage = "Запись успешно завершена";
        FileTextWorker.writeReversedTextToFile(stringReader, stringWriter);
        Assertions.assertTrue(logCaptor.getInfoLogs().contains(expectedInfoMessage));

    }

    @Test
    void testReverseStringSuccess() {
        String reverseResult = FileTextWorker.reverseString("Amazon");
        String reverseResultNumbers = FileTextWorker.reverseString("654321");
        Assertions.assertEquals("nozamA", reverseResult);
        Assertions.assertEquals("123456", reverseResultNumbers);
    }

    @Test
    void testReverseStringMethodSmallSuccess() {
        String reverseResult = FileTextWorker.reverseStringSmall("Amazon");
        String reverseResultNumbers = FileTextWorker.reverseStringSmall("654321");
        Assertions.assertEquals("nozamA", reverseResult);
        Assertions.assertEquals("123456", reverseResultNumbers);
    }
}