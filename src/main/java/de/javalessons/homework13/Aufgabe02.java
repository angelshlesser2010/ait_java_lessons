package de.javalessons.homework13;

import java.util.ArrayList;

public class Aufgabe02 {
    public static void main(String[] args) {
        /*
         * Создайте ArrayList со строками: "один", "два", "три", "четыре", "пять".
         * Напишите программу, которая переставляет
         * элементы списка в обратном порядке.
         * Выведите список до и после перестановки.
         */

        //Создаем ArrayList со строками
        ArrayList<String> listString=new ArrayList<>();
        listString.add("раз");
        listString.add("два");
        listString.add("три");
        listString.add("четыре");
        listString.add("пять");

        //Выводим список "до"
        System.out.println("Исходный массив");
        System.out.println(listString);

        //#1
        for(int i=0; i<listString.size()/2; i++)
        {
            String str= listString.get(i);
            listString.set(i,listString.get(listString.size()-i -1));
            listString.set(listString.size() -i -1, str);
        }
        System.out.println("Преобразрванный массив");
        System.out.println(listString);


        //#2
        int sizeOfList = listString.size();
        for (int i = sizeOfList - 1; i >= 0; i--) {
            System.out.println("Reversed: " + listString.get(i));
        }
        for (int i = 0; i < sizeOfList; i++) {
            System.out.println("Original" + listString.get(i));
        }


        //#3
        ArrayList<String> numbersReverse = new ArrayList<>();
        for (int i = listString.size() - 1; i >= 0; i--) {
            numbersReverse.add(listString.get(i));
        }
        System.out.println (numbersReverse);
    }

}
