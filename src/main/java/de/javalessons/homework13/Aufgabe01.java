package de.javalessons.homework13;

import java.util.ArrayList;

public class Aufgabe01 {
    public static void main(String[] args) {
        /*
        Создайте новый ArrayList для строк. Добавьте в него следующие строки: "Яблоко",
        "Банан", "Черешня", "Дыня", "Ежевика".
        Удалите из списка "Дыня". Вставьте на
        место "Черешня" строку "Вишня".
        Выведите содержимое списка.
         */

        //Создайте новый ArrayList для строк
        ArrayList<String> fruits = new ArrayList<String>();

        //Добавляем в него элементы
        fruits.add("Apple");
        fruits.add("Banan");
        fruits.add("Sweet Cherry");
        fruits.add("Melon");
        fruits.add("Blackberry");

        System.out.print("ArrayFruits contains the next elements" + fruits);
        System.out.println();

        //Удалите из списка "Дыня"
        fruits.remove(3);

        // Замена второго элемента
        fruits.set(2, "Cherry");

        System.out.print("ArrayFruits  contains the next elements after change" + fruits);

    }
}
