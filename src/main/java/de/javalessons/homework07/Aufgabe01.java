package de.javalessons.homework07;

import java.util.Scanner;

public class Aufgabe01 {

    /*
     Напишите программу, которая находит минимальную и максимальную цифру
      в числе введенном с клавиатуры используя циклы. Пример: вводится число 5987.
      На консоль выводится: "минимальная цифра 5, максимальная 9".
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число: ");
        int number = scanner.nextInt();

        int minDigit = 9;
        int maxDigit = 0;

        while (number > 0){
            int digit = number%10;
            System.out.println("digit = " + digit);
            if(digit < minDigit){
                minDigit = digit;
            }
            if(digit > maxDigit){
                maxDigit = digit;
            }
            number = number/10;
            System.out.println("number: " + number);
            System.out.println("---------------------");
        }
        System.out.println("Max: " + maxDigit + " Min:" + minDigit);
    }
}
