package de.javalessons.homework07;

import java.util.Scanner;

public class Aufgabe03 {
    public static void main(String[] args) {
        /*
        Напишите программу, которая принимает от пользователя последовательность чисел
        (количество заранее не известно)
        и вычисляет их среднее значение.
        Пользователь сам определяет когда будет остановлен ввод чисел и выведено среднее
         значение для уже введенных, введя 000.
         Пример: Пользователь вводит с клавиатуры "10", "20", "30", "000".
         Ввод в консоль: "Среднее значение 20"
         */

        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число: ");
        int number = scanner.nextInt();

        int sum = 0;
        int countNumber = 0;
        double result = 0;

        while (number != 0) {
            sum = sum + number;
            countNumber++;
            System.out.println(" number: " + number);
            System.out.println("sum: " + sum);
            System.out.println("countNumber: " + countNumber);
            System.out.print("Введите следующее число: ");
            number = scanner.nextInt();
            //countNumber = countNumber+1
            System.out.println("----------------" + countNumber);
        }
        if (countNumber == 0) {
            System.err.println("Вы не ввели ни одного числа");
        } else {
            result = (double) sum / countNumber;
            System.out.println("Среднее число: " + result);
        }
        /**Scanner sc = new Scanner(System.in);
         System.out.println("Введите числа");
         int number = 10;
         int sum = 0;
         int schet = 0;
         while (number != 0) {
         number = sc.nextInt();
         sum = sum + number;
         schet = schet + 1;
         if (number == 0) schet = schet - 1;
         System.out.println(sum);
         }
         System.out.printf("Среднее из %d чисел равно %d", schet, sum / schet);*/
    }
}









