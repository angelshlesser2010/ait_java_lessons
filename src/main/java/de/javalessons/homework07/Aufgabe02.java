package de.javalessons.homework07;

import java.util.Scanner;

public class Aufgabe02 {
    public static void main(String[] args) {
        /*
        Напишите программу, которая принимает число N и вычисляет сумму всех чисел от 1 до N,
         используя цикл. Пример:вводится число 10.
         На консоль выводится (10+9+8+7+6+5+4+3+2+1): "Сумма чисел 55"
         */
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число: ");
        int number = scanner.nextInt();

        int sum = 0;

        for(int i=1; i <= number; i++){
            sum = sum + i;
            System.out.println("i = " + i);
            System.out.println("sum = " + sum);
            System.out.println("--------");
        }
        System.out.println("---------------------------");
        System.out.println("Сумма его чисел равна: " + sum);
    }
}
