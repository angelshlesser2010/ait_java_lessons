package de.javalessons.calculator;

public class Calculator {

    private int add(int numberOne, int numberTwo){
        return numberOne + numberTwo;
    }

    private int substruct (int numberOne, int numberTwo){
        return numberOne - numberTwo;
    }

    public int makeOperation(int numberOne, int numberTwo, String operation){
        int result = 0;

        switch (operation){
            case "add":
                result = add(numberOne, numberTwo);
                break;
            case "substruct":
                result = substruct(numberOne, numberTwo);
                break;
            default:
                System.out.println("ERROR");
                return -1;
        }
        return result;
    }
}
