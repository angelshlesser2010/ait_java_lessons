package de.javalessons.homework08;

public class Aufgabe01 {
    public static void main(String[] args) {
        /*
        Напишите программу, которая будет считать от 10 до 1
        в обратном порядке, используя цикл do-while. 10,9,8,7...1
         */
        int x = 11;
        do {
            x--;
            System.out.println(" " + x);
        }
        while (x != 1);

    }
}
