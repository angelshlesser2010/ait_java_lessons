package de.javalessons.homework08;

import java.util.Random;
import java.util.Scanner;

public class Aufgabe03 {

    /**
     * Напишите простую игру, в которой компьютер загадывает число от 1 до 100,
     * а пользователь пытается его угадать. Используйте цикл do-while, чтобы повторять игру,
     * пока пользователь не угадает число.
     */
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int max = 100;
        int min = 1;
        int computerNumber = (int) ((Math.random()* (max - min)) + min);
        int myTry;
        do {

            System.out.print("Компьютер загадал число от 1 до 100: ");
            myTry = scanner.nextInt();
            if (computerNumber != myTry) {
                System.out.println("Не угадили, попробуйте еще раз.");
                //System.out.println("Компьютер загадал: " + computerNumber);
            }
        }
        while (computerNumber != myTry);
        System.out.println("Угадали !!!");



    }
}

