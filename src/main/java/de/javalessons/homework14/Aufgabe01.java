package de.javalessons.homework14;

import java.util.ArrayList;

public class Aufgabe01 {
    public static void main(String[] args) {
        /*
        Создайте два списка типа String: первый содержит имена,
        второй - фамилии. Создайте третий список,
        который будет содержать полные имена (имя и фамилия),
        объединив два предыдущих списка. Выведите третий список на экран
         */
        ArrayList<String> imena = new ArrayList<>();
        ArrayList<String> familii = new ArrayList<>();
        ArrayList<String> polnyeimena = new ArrayList<>();
        imena.add("Иванов");
        imena.add("Петров");
        imena.add("Сидоров");
        imena.add("Соболев");
        imena.add("Вассерман");
        familii.add("Петр");
        familii.add("Иван");
        familii.add("Олег");
        familii.add("Василий");
        familii.add("Николай");
        if (imena.size() != familii.size() || imena.isEmpty() || familii.isEmpty()) {
            System.err.println("Данные не могут быть обработаны");
        }
        else {
            for (int i = 0; i < imena.size(); i++) {
                String full = imena.get(i) + " " + familii.get(i);
                polnyeimena.add(full);
            }
            System.out.println(polnyeimena);
        }

    }
}
