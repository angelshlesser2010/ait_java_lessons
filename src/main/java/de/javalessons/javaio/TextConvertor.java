package de.javalessons.javaio;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class TextConvertor {

    private static final Logger LOGGER = LoggerFactory.getLogger(TextConvertor.class);


    public String convertToLowerCase(Reader input, Writer output) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(input);
        BufferedWriter bufferedWriter = new BufferedWriter(output);
        String line;
        String lowerCaseWord = null;
        line = bufferedReader.readLine();
        String[] words = line.split("\\s+");
        for (int i = 0; i < words.length; i++) {
            lowerCaseWord = words[i].toLowerCase();
            bufferedWriter.write(lowerCaseWord);
            bufferedWriter.write(" ");
        }
        bufferedWriter.newLine();
        bufferedWriter.flush();
        LOGGER.info("Запись успешно завершена");
        return lowerCaseWord;

    }

}
