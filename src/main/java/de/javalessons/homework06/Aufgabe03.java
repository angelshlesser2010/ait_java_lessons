package de.javalessons.homework06;

import java.util.Scanner;

public class Aufgabe03 {
    public static void main(String[] args) {
        /*
        Написать программу, которая принимает на вход число
         n и выводит все простые числа до n.
         10-->1,3,5,7
         */

        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число: ");
        int numberInt = scanner.nextInt();

        if(numberInt <= 0){
            System.err.println("Число должно быть положительным");
        }
        else {

            for (int i=2; i<numberInt-1; i++){
            boolean result = true;
                for (int y=2; y <= 1+i/2; y++ ){
                    if(i % y == 0){
                        result = false;
                        break;
                    }
                }
                if(result){
                    System.out.printf("%d ", i);
                }

            }
        }


        //#2
        int checkNumber;
        boolean result=true;

        for (int i=2; i<=numberInt/2; i++) {
            checkNumber = numberInt % i;
            if (checkNumber == 0) {
                result = false;
                break;
            }
            //TODO Вывод чисел на экран
        }
        if(result) {
            System.out.println(numberInt + " - простое число");
        } else {
            System.out.println(numberInt + " - составное число");
        }
    }
}
