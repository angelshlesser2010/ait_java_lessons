package de.javalessons.homework12;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Aufgabe01 {
    public static void main(String[] args) {
        /*
        Дан массив целых чисел.
        Используя цикл for-each и изученные структуры
         данных, определите число, которое
          встречается чаще всего в массиве.
         */
        Scanner scanner = new Scanner(System.in);
        System.out.println("Tell me how long should be the array?");
        int arrayLength = scanner.nextInt();
        int[] myArrayOne = new int[arrayLength];
        System.out.println("Enter " + arrayLength + " numbers");
        for (int i = 0; i < arrayLength; i++) {
            myArrayOne[i] = scanner.nextInt();
        }
        int mostFrequent = myArrayOne[0];// the most frequent number
        int maxFrequency = 1; // max frequency
        int frequency = 1; // current frequency
        int currentIndex = 0;
        for (int num : myArrayOne) {  // for each is here
            if (currentIndex > 0 && num == myArrayOne[currentIndex - 1]) {
                frequency++;
            } else {
                frequency = 1;
            }
            if (frequency > maxFrequency) {
                mostFrequent = num;
                maxFrequency = frequency;
            } else if (frequency == maxFrequency) {
                mostFrequent = Math.min(mostFrequent, num);
            }
            currentIndex++;
        }
        System.out.println("Число " + mostFrequent + " встречается  чаще всего в массиве: . ");
        System.out.print("Массив: ");
        for (int i = 0; i < myArrayOne.length; i++)
            System.out.print(" " + myArrayOne[i]);


        //#2
        int[] array = new int[]{1, 2, 8, 4, 2, 6, 8, 3, 4, 5, 8, 6, 7, 8, 9, 9, 9, 9};
        ArrayList<Integer> listInt = new ArrayList<>();
        Arrays.sort(array);//сортируем массив
        System.out.println(array.length);
        int count = 0;
        for (int num : array)//Выводим массив
        {
            System.out.printf("%d ", num);
        }
        int maxchislo = 0;//определяем максимальное число из массива заданных чисел
        for (int num : array) {
            if (num > maxchislo) {
                maxchislo = num;
            }
        }
        int minchislo = 999;//определяем минимальное число из массива заданных чисел
        for (int l : array) {
            if (l <= minchislo) {
                minchislo = l;
            }
        }
        System.out.println("\n");
        System.out.println(maxchislo + " " + minchislo);//выводим минимальное и максимальное числа массива
        for (int i = minchislo; i <= maxchislo; i++) {
            count = 0;
            for (int j = 0; j < array.length; j++)//Подсчитываем количество повторений каждого из чисел
            {
                if (i == array[j]) {
                    count++;
                }
            }
            listInt.add(i);//Добавляем встречающиеся числа в массив
            listInt.add(count);//Добавляем число поворений каждого из чисел в другой массив
        }
        System.out.println(listInt);
        int[] arr2 = new int[listInt.size()];
        for (int j = 0; j < listInt.size(); j++) {
            arr2[j] = listInt.get(j);
        }
        int max = 0;
        int ind = 0;
        for (int i = 0; i < arr2.length; i++) {
            if (i % 2 != 0) {
                if (arr2[i] >= max) {
                    max = arr2[i];
                    ind = i;
                }
            }
        }
        System.out.println("Максимальное число повторений:" + max + " раз" + "числа  " + arr2[ind - 1]);
    }
}



