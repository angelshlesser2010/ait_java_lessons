package de.javalessons.homework12;

import java.util.ArrayList;

public class home01 {
    public static void main(String[] args) {
    /*
    Задание 1: Создайте новый ArrayList для строк.
    Добавьте в него следующие строки: "Яблоко", "Банан",
    "Черешня", "Дыня", "Ежевика".
    Удалите из списка "Дыня".
    Вставьте на место "Черешня" строку "Вишня".
    Выведите содержимое списка.
     */
        ArrayList<String> list = new ArrayList<>();
        list.add("Яблоко");
        list.add("Банан");
        list.add("Черешня");
        list.add("Дыня");
        list.add("Ежевика");
        System.out.println(list);
        //Удалите из списка "Дыня".
        list.remove("Дыня");
        System.out.println("-------");
        System.out.println(list);
        //Вставьте на место "Черешня" строку "Вишня".
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals("Черешня")) {
                list.set(i, "Вишня");
            }
        }
        // Простой если знаем индех list.set(2,"Вишня");
        System.out.println("-------");
        System.out.println(list);
    }
}
