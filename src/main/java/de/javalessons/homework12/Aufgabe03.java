package de.javalessons.homework12;

import java.util.Random;
import java.util.Scanner;

public class Aufgabe03 {
    public static void main(String[] args) {
        /*
         * Создайте программу, которая симулирует бросок монеты.
         * Пользователь вводит, сколько раз хочет бросить монету,
         * а программа считает и показывает, сколько раз выпал орел и решка. Используйте do-while для повторения эксперимента до тех пор, пока пользователь не решит завершить.
         */
        Scanner scan = new Scanner(System.in);
        int numberOfCoinTosses;
        int max = 10; //eagle 6-10;
        int min = 1; // tails 1-5
        int countpEagle = 0;
        int counttails = 0;
        boolean flag = false;

        do {
            System.out.println("How many times do you want to toss a coin?: ");
            numberOfCoinTosses = scan.nextInt();
            for (int i = 1; i <= numberOfCoinTosses; i++) {
                int result = (int) (Math.random() * (max - min) + min);
                if (result > 5) {
                    countpEagle++;
                } else if (result <= 5) {
                    counttails++;
                }
            }
            System.out.println("Do you want to flip the coin again or end the game?.\n Enter 1 If you want to continue the game " +
                    "or Lead any other number if you want to end the game");
            int resultend = scan.nextInt();
            if (resultend == 1) {
                flag = true;
            } else if (resultend != 1) {
                flag = false;
            }
        } while (flag);

        System.out.printf("\nResult: Eagle - %d, tails - %d", countpEagle, counttails);



        Random random =new Random();
        System.out.println("Для продолженичя работы нажимайте Enter");
        int orel=0;
        int reshka=0;
        int i=0;
        do {
            int randomNumber =random.nextInt(2);
            System.out.println("Процесс выполяется");
            System.out.println("Нажмите q для остановки процесса");
            if(randomNumber==1)
            {
                orel++;
            }
            else
            {
                reshka++;
            }
            i++;
            System.out.println(i);
        }
        while(!scan.nextLine().equals("q"));
        System.out.println("Процесс остановлен");
        System.out.printf("Выпало: " + orel +" орлом и " + reshka + " решка");

    }
}
