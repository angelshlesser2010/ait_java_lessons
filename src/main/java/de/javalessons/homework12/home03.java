package de.javalessons.homework12;

import java.util.ArrayList;

public class home03 {
    public static void main(String[] args) {
    /*
    Создайте ArrayList из чисел: 1, 2, 3, 4, 5, 6, 7, 8, 9, 10.
    Получите подсписок из этого списка, который содержит только числа от 4 до 8.
    Выведите этот подсписок.
     */
        ArrayList<Integer> number = new ArrayList<>();
        //Создайте ArrayList из чисел: 1, 2, 3, 4, 5, 6, 7, 8, 9, 10.
        /*number.add(1);
        number.add(2);
        number.add(3);
        number.add(4);
        number.add(5);
        number.add(6);
        number.add(7);
        number.add(8);
        number.add(9);
        number.add(10);*/
        for (int i =1; i<=10; i++){
            number.add(i);
        }
        System.out.println(number);
        //Получите подсписок из этого списка, который содержит только числа от 4 до 8.
        ArrayList<Integer> numberNew = new ArrayList<>();
        /*for (int element:number){
            if (element>3&&element<=8){
                numberNew.add(element);
            }
        }
         */
        for (int i = 3; i < 8; i++) {
            numberNew.add(number.get(i));
        }
        System.out.println(numberNew);
    }
}
