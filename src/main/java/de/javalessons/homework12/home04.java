package de.javalessons.homework12;

import java.util.ArrayList;

public class home04 {
    public static void main(String[] args) {
    /*
    Задание 4: Создайте два ArrayList со строками.
    Найдите элементы, которые присутствуют в обоих списках
    и создайте новый список на их основе.
    Выведите новый список.
     */

        ArrayList<String> list1 = new ArrayList<>();
        list1.add("Hello");
        list1.add("Good");
        list1.add("Дождь");
        list1.add("Страна");
        list1.add("Ветер");
        list1.add("Солнце");

        ArrayList<String> list2 = new ArrayList<>();
        list2.add("Ночь");
        list2.add("Ветер");
        list2.add("Небо");
        list2.add("Java");
        list2.add("Good");
        list2.add("Кот");
        System.out.println(list1);
        System.out.println(list2);
        //Вариант 1
        //ArrayList<String> listNew = new ArrayList<>(list1);
        //listNew.retainAll(list2);
        //System.out.println("Общие элементы: " + listNew);
        // Вариант 2
        /*ArrayList<String> temp = new ArrayList<>();
        for (String element : list1) {
            if (list2.contains(element)) {
                temp.add(element);
            }
        }
         */
        //System.out.println("Общие элементы: " + temp);
        //Вариант 3
        ArrayList<String> temp = new ArrayList<>();
        for (String element1:list1 ){
            for (String element2:list2){
                if (element1.equals(element2)){
                    temp.add(element1);
                }
            }
        }
        System.out.println("Общие элементы: " + temp);
    }
}
