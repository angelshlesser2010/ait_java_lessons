package de.javalessons.homework12;

import java.util.ArrayList;
import java.util.Collections;

public class home02 {
    public static void main(String[] args) {
        /*
        Задание 2: Создайте ArrayList со строками: "один", "два", "три", "четыре", "пять".
        Напишите программу, которая переставляет элементы списка в обратном порядке.
        Выведите список до и после перестановки.
         */
        ArrayList<String> list = new ArrayList<>();
        list.add("один");
        list.add("два");
        list.add("три");
        list.add("четыре");
        list.add("пять");
        //Выведите список до и после перестановки.
        System.out.println(list);
        //Напишите программу, которая переставляет элементы списка в обратном порядке.
        //Collections.reverse(list);
        /*int size = list.size();
        for(int i=0; i<size/2;i++){
            String temp = list.get(i);
            list.set(i, list.get(size - 1 - i));
            list.set(size - 1 - i, temp);
        }
         */
        int left = 0;
        int right = list.size() - 1;

        while (left < right) {
            String temp = list.get(left);
            list.set(left, list.get(right));
            list.set(right, temp);

            left++;
            right--;
        }
        System.out.println(list);

    }
}
