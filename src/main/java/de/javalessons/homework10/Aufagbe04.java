package de.javalessons.homework10;

public class Aufagbe04 {
    public static void main(String[] args) {
        /*
        Написать программу,
        которая возвращает сумму всех
        элементов массива.
         */
        int[] array = {5, 10, 15, 20, 25};

        int sum = 0;

        for (int i=0; i< array.length; i++) {
            sum += array[i]; //sum = sum + array[i];
        }

        System.out.println("Сумма всех элементов массива: " + sum);
    }
}
