package de.javalessons.interfaces;

public interface MediaPlayer {

        void stop(String title);
        void rewind(String title);
        void pause(String title);

}
