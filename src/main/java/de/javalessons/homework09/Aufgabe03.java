package de.javalessons.homework09;

import java.util.Scanner;

public class Aufgabe03 {
    public static void main(String[] args) {
        /*
        Напишите программу, которая выводит шахматную доску заданного размера
         N х N, используя символы "#" и " ".
         */


        /**
         Scanner scanner = new Scanner(System.in);
         System.out.print("Enter the chessdesk size: ");
         int chessDeskSize = scanner.nextInt();
         for (int i = 0; i < chessDeskSize; i++) { /*i - горизонтальные строки доски*/
       // for (int j = 0; j < chessDeskSize; j++) { /*j- вертикальные строки *2 для симетрии доски */
         //   if ((i + j) % 2 == 0) {
           //     System.out.print("#\t");
            //} else {
             //   System.out.print("\t");
          //  }
       // }
       // System.out.println(); /* Переход на новую строку после каждой строки доски */
    //}
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the chessdesk size: ");
        int chessDeskSize = scanner.nextInt();// Задайте размер доски
        for (int i = 0; i < chessDeskSize; i++) {
            for (int j = 0; j < chessDeskSize; j++) {
                char square = (i + j) % 2 == 0 ? '\u263b' : '\u263A';
                System.out.print(square + " ");
            }
            System.out.println();
        }
    }
}
