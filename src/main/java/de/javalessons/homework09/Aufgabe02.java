package de.javalessons.homework09;

import java.util.Scanner;

public class Aufgabe02 {
    public static void main(String[] args) {
        /*
        Реализуйте программу, которая возводит число в степень
         без использования встроенной функции Math.pow().
         Попробуйте реализовать алгоритм быстрого возведения в степень
         для улучшения эффективности.
         */
        Scanner scan =new Scanner(System.in);
        int result=1;
        System.out.println("введите число");
        int number= scan.nextInt();
        System.out.println("введите степень >= 0");
        int exponent=scan.nextInt();
        if(exponent < 0){
            System.err.println(" Степень  не должна быть отрицательной");
        }
        else {
            for (int i = 0; i < exponent; i++) {
                result *= number;
            }
            System.out.printf("%d", result);
        }
    }

}
