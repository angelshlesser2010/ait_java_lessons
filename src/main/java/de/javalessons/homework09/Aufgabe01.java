package de.javalessons.homework09;

import java.util.Scanner;

public class Aufgabe01 {
    public static void main(String[] args) {
        /*
        Напишите программу, которая выводит строку в обратном порядке.
        Ваша программа должна пройти по всей строке с конца до начала и
        вывести её на экран.
         */
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter something: "); // prüfe was für Input
        String stroka=scanner.nextLine();
        //Hallo
        //01234
        System.out.println("Result: ");
        for(int i=stroka.length()-1; i>=0; i--){
            char letter = stroka.charAt(i);
            System.out.printf("%s", letter);
        }
        System.out.println("");
    }
}
