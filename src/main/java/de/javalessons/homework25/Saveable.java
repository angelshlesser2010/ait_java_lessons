package de.javalessons.homework25;

public interface Saveable {
    void save(String filename);
    void load(String filename);
}
