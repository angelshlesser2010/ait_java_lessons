package de.javalessons.homework25;

public interface Vehicle {
    void drive();

    void stop();

    void fuelUp();

}
