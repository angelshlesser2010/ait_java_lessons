package de.javalessons.homework18;

public class Aufgabe03 {
    public static void main(String[] args) {
        /*
        Создайте метод, который принимает
        переменное число аргументов типа double
         и возвращает их среднее значение.
         */
        double average1 = calculateAverage(1.0, 2.0, -3.0);
        System.out.println("Среднее значение 1: " + average1);
        System.out.println();
        double average2 = calculateAverage(4.0, 5.0, 6.0, 7.0);
        System.out.println("Среднее значение 2: " + average2);
        System.out.println();
        double average3 = calculateAverage();
        System.out.println("Среднее значение 3: " + average3);
        System.out.println();
        double average4 = calculateAverage(Double.MAX_VALUE,  Double.MAX_VALUE);
        System.out.println("Среднее значение 4: " + average4);
        System.out.println();
        double average5 = calculateAverage( Double.MIN_VALUE,  Double.MIN_VALUE,
                 Double.MIN_VALUE,  Double.MIN_VALUE, Double.MIN_VALUE);
        System.out.println("Среднее значение 5: " + average5);
        System.out.println();

        double average6 = calculateAverage(null);
        System.out.println("Среднее значение 6: " + average6);
        System.out.println();
    }

    public static double calculateAverage(double... numbers) {
        if (numbers == null || numbers.length == 0) {
            System.err.println("Среднее значение не может быть вычислено для пустого списка чисел.");
            return 0.0; // Возвращаем 0.0 в случае ошибки или пустого списка чисел
        }
        double sum = 0.0;
        for (double number : numbers) {
            sum += number;
        }
        return sum / numbers.length;
    }

}
