package de.javalessons.homework18;

public class Aufgabe02 {
    public static void main(String[] args) {
        /*
        Создайте метод, который принимает переменное число аргументов типа String и возвращает
        одну строку, составленную из всех переданных строк,
        разделенных пробелами.
         */
        String result = concat_strings("first", "second", "third", "fourth");
        System.out.println(result);

        System.out.println(concat_strings("first", "second", "third", "fourth"));
        System.out.println(concat_strings("first", "second", "third"));
        System.out.println(concat_strings("first", "second"));
        System.out.println(concat_strings());
    }

    static String concat_strings(String... strings) {
        if(strings.length == 0){
            return "ERROR";
        }
        String total = "";
        for (String string : strings) {
            total = total + string + " ";
        }
        // #2 total = total.substring(0, total.length()-1);
        total = total.trim();
        return total;
    }
}

