package de.javalessons.zoosoftware;

public interface Feeding {

    //Метод, выводящий информацию
    // о том, какое животное что ест.
    void eat();
}
