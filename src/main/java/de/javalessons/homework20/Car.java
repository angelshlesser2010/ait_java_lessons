package de.javalessons.homework20;

public class Car {
    /*
    Создайте класс Car с приватными полями make (марка),
     model (модель) и engine (двигатель).
     Двигатель должен быть отдельным классом Engine
     с полями type (тип) и power (мощность),
     а также сеттерами и геттерами.
     В классе Car добавьте методы
     для установки и получения двигателя
     */

    private String make;

    private String model;

    private Engine engine;

    public Car(String make, String model, Engine engine) {
        this.make = make;
        this.model = model;
        this.engine = engine;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    @Override
    public String toString() {
        return "Car{" +
                "make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", engine=" + engine +
                '}';
    }
}
