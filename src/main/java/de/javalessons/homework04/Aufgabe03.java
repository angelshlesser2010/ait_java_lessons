package de.javalessons.homework04;

import java.util.Scanner;

public class Aufgabe03 {
    public static void main(String[] args) {

        /*
        Разработайте "игру в угадай число". Компьютер "загадывает" число от 1 до 5,
        и пользователь должен угадать это число. Если пользователь вводит число,
        которое больше загаданного, программа выводит "слишком большое".
        Если пользователь вводит число, которое меньше загаданного,
        программа выводит "слишком маленькое". Если пользователь угадывает число,
        программа выводит "Поздравляем, вы угадали число!". Используйте if-else для сравнения чисел.
         */

        int number;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите  число 1-5 ");
        number = scanner.nextInt();

        int max = 5;
        int min = 1;

        int numberComputer = (int) ((Math.random() * (max - min)) + min);

        switch (number){
            case 1 -> {
                if (numberComputer == 1){
                    System.out.println("Поздравляем, вы угадали число!");
                }
                else if (numberComputer > number)  {
                    System.out.println("Число компьютера больше " + numberComputer);
                }

            }
            case 2 -> {
                if (numberComputer == number){
                    System.out.println("Поздравляем, вы угадали число!");
                }
                else if (numberComputer > number)  {
                    System.out.println("Число компьютера больше " + numberComputer);
                }
                else {
                    System.out.println("Число компьютера меньше " + numberComputer);
                }

            }
            case 3 -> {
                if (numberComputer == 3){
                    System.out.println("Поздравляем, вы угадали число!");
                }
                else if (numberComputer > number)  {
                    System.out.println("Число компьютера больше " + numberComputer);
                }
                else {
                    System.out.println("Число компьютера меньше " + numberComputer);
                }
            }
            case 4 -> {
                if (numberComputer == 4){
                    System.out.println("Поздравляем, вы угадали число!");
                }
                else if (numberComputer > number)  {
                    System.out.println("Число компьютера больше " + numberComputer);
                }
                else {
                    System.out.println("Число компьютера меньше " + numberComputer);
                }
            }
            case 5 -> {
                if (numberComputer == number){
                    System.out.println("Поздравляем, вы угадали число!");
                }
                else {
                    System.out.println("Число компьютера меньше " + numberComputer);
                }
            }

                default -> System.err.println("ОШИБКА");
        }

        // Вариант 2
        if (numberComputer == number){
            System.out.println("Поздравляем, вы угадали число!");
        }
        else if (number >= 1 && number >= 5 && numberComputer > number){
            System.out.println("Число компьютера больше " + numberComputer);

        }
        else if (number >= 1 && number >= 5 && numberComputer < number){
            System.out.println("Число компьютера меньше " + numberComputer);

        }
        else {
            System.err.println("ОШИБКА");
        }


    }
}
