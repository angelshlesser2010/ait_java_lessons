package de.javalessons.homework03;

import java.util.Scanner;

public class Aufgabe01 {
    /*
    Напишите программу, которая получает на вход два числа и
    выводит большее из них. Используйте логические операторы в вашем условии.
     */

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
// Цены на продукты
        int cokePrice = 3;
        int waterPrice = 2;
        int chocolatePrice = 5;
        System.out.println(
                "Доступные продукты:");
        System.out.println(
                "1. Кока-Кола - " + cokePrice + " евро");
        System.out.println(
                "2. Вода - " + waterPrice + " евро");
        System.out.println(
                "3. Шоколад - " + chocolatePrice + " евро");
        System.out.print(
                "Выберите продукт (введите номер): ");
        int productChoice = scanner.nextInt();
        int productPrice = 0;
        String productName = "";
// Определение выбранного продукта и его стоимости
        switch (productChoice) {
            case 1:
                productName =

                "Кока-Кола";
                productPrice = cokePrice;
                break;
            case 2:
                productName =
                        "Вода";
                productPrice = waterPrice;
                productPrice = waterPrice;
                break;
            case 3:
                productName = "Шоколад";
                productPrice = chocolatePrice;
                productPrice = chocolatePrice;
                break;
            default:
                System.out.println(
                        "Неверный выбор продукта.");
                return;
        }
        System.out.println(
                "Введите количество денег: ");
        int moneyAmount = scanner.nextInt();
        if (moneyAmount < productPrice) {
            int moneyNeeded = productPrice - moneyAmount;
            System.out.println("Недостаточно денег. Пожалуйста, внесите еще " + moneyNeeded + " евро.");


        }
    }
}
