package de.javalessons.homework03;

public class Aufgabe03 {

    /*
    Промежутки чисел: Напишите программу, которая проверяет,
    находится ли введенное число в определенном диапазоне.
    Например, вы можете проверить, является ли число отрицательным,
    положительным или нулевым, используя логические операторы.
     */

    public static void main(String[] args) {

        int number = 0;

        if(number > 0 && number!= 0){
            System.out.println("Число "   + number + " положительное");
        }
        else if(number < 0 && number != 0 ) {
            System.out.println("Число "   + number + " отрицательное");
        }
        else {
            System.out.println("Число "   + number + " равно 0");
        }

    }
}
