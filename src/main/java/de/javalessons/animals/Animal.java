package de.javalessons.animals;

public interface Animal {
    void eat();
    void makeSound();
}
