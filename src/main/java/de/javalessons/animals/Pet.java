package de.javalessons.animals;

public interface Pet {
    void beFriendly();
}
