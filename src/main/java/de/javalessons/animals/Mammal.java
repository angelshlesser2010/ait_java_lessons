package de.javalessons.animals;

public interface Mammal extends Animal{
    void feedYoung();
}
