package de.javalessons.animals;

public class TestAnimal {
    public static void main(String[] args) {
        Sparrow sparrow = new Sparrow();
        sparrow.eat();
        sparrow.makeSound();
        sparrow.fly();

        System.out.println(sparrow.getColor());
        System.out.println(sparrow.getWeight());
        System.out.println("----------");

        Dog dog = new Dog();
        dog.makeSound();
        dog.feedYoung();
        dog.eat();
        dog.beFriendly();


        //Используем полиморфизм
        Animal animal = dog; //Animal animal = new Dog();
        animal.makeSound();

        Mammal mammal = dog;
        mammal.feedYoung();

        Pet pet = dog;
        pet.beFriendly();

        //Переопределяем переменную
        animal = sparrow;
        animal.makeSound();
        animal.eat();





    }
}
