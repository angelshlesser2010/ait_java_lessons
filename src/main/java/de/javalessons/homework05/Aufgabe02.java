package de.javalessons.homework05;

import java.util.Scanner;

public class Aufgabe02 {
    public static void main(String[] args) {

        /*
         Напишите программу, которая принимает три числа и выводит true,
         если сумма квадратов первых двух равна квадрату третьего.
         */

        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число 1:");
        int numberOne = scanner.nextInt();
        System.out.print("Введите число 2:");
        int numberTwo = scanner.nextInt();
        System.out.print("Введите число 3:");
        int numberTree = scanner.nextInt();
        boolean result = true;

        if ((numberOne * numberOne + numberTwo * numberTwo)
                != (numberTree * numberTree)) {
            result = false;
        }

        System.out.println(result);


    }
}
