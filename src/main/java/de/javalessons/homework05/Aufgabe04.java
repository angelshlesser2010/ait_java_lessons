package de.javalessons.homework05;

import java.util.Scanner;

public class Aufgabe04 {
    public static void main(String[] args) {
        /*
        Напишите программу, которая принимает на вход три числа и выводит true,
        если они упорядочены в возрастающем или убывающем порядке.
         */

        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число 1:");
        int numberOne = scanner.nextInt();
        System.out.print("Введите число 2:");
        int numberTwo = scanner.nextInt();
        System.out.print("Введите число 3:");
        int numberTree = scanner.nextInt();

        boolean result = false;

        if ((numberTwo > numberOne && numberTwo < numberTree)
                || (numberTwo < numberOne && numberTwo > numberTree)) {
            result = true;
        }
        System.out.println(result);

    }
}
