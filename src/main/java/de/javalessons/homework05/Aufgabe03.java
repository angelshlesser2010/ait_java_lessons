package de.javalessons.homework05;

import java.util.Scanner;

public class Aufgabe03 {

    public static void main(String[] args) {
        /*
         Напишите программу, которая проверяет,
         делится ли одно заданное число на другое без остатка.
         */
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число 1:");
        int numberOne = scanner.nextInt();
        System.out.print("Введите число 2:");
        int numberTwo = scanner.nextInt();

        if (numberTwo == 0) {
            System.err.println("Деление на 0");
        } else {
            boolean result = numberOne % numberTwo == 0;
            System.out.println(result);
        }
    }
}
