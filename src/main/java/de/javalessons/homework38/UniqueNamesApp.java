package de.javalessons.homework38;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class UniqueNamesApp {

    private static Set<String> namesSet;

    public static void main(String[] args) {

        namesSet = new HashSet<>();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите элемент имя или СТОП для вывода на экран");

        while (true) {
            String name = scanner.nextLine();
            if (name.equalsIgnoreCase("СТОП")) {
                break;
            }
            boolean result = namesSet.add(name);
            //if(namesSet.add(name))
            if (result) {
                System.out.println("Элемент имя " + name + " был успешно добавлен");
            } else {
                System.out.println("Элемент  имя " + name + " НЕ добавлен");
            }
        }
        scanner.close();
        printNames();

    }

    private static void printNames() {
        if (namesSet.isEmpty()) {
            System.out.println("Список уникальных имен пуст");
        } else {
            System.out.println("Уникальные имена введенные пользователем: ");
            for (String name : namesSet) {
                System.out.println(name);
            }
        }


    }
}
