package de.javalessons.homework19;

public class Book {
        /*
        Создайте класс Book с полями: автор, название,
         количество страниц и методом read(),
         который выводит сообщение о том, что книгу читают.
         */
        int pageNumbers;
        String bookTitle;
        String authorName;

    public Book(int pageNumbers, String bookTitle, String authorName) {
        this.pageNumbers = pageNumbers;
        this.bookTitle = bookTitle;
        this.authorName = authorName;
    }

    public void read() {
        System.out.println("Somebody reads this book: " + "'" + bookTitle + "'" + " by " + authorName + " Pages: " + pageNumbers);
    }



}
