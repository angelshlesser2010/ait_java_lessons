package de.javalessons.homework19;

public class BankAccount {
    /*
    Создайте класс "Банковский счет"
     Поля: номер счета, баланс.
     Методы: конструкторы,
     deposit(double amount) - добавляет сумму на счет,
     withdraw(double amount) - снимает сумму со счета.
      Создайте класса "Тест"
      В этом классе создайте несколько объектов
       вышеуказанного класса и вызовите их методы.
     */
    private int accountnumber;
    private double balance;

    final double  limit = 100;

    public BankAccount(int accountnumber, double balance) {
        this.accountnumber = accountnumber;
        this.balance = balance;
    }

    public void deposit(double amount){
        balance = balance + amount;
        System.out.println("Вы внесли на счет № " + accountnumber + " " + amount + " евро. На вашем счете " + balance + " евро.");
    }

    public void withdraw(double amount){
        if(checkBankAccount(amount)){
            balance = balance - amount;
            System.out.println("Вы сняли со счета № " + accountnumber + " " + amount + " евро. На вашем счете " + balance + " евро.");
        }
        else {
            System.err.println("На вашем счете недостаточно средств для снятия " + amount + " евро или превышен лимит на снятие денег");
        }
    }

    private boolean checkBankAccount(double amount){
        if(amount > limit || amount > balance ){
            return false;
        }
        return true;
    }

    public double getBalance() {

        return balance;
    }

    public int getAccountnumber() {
        return accountnumber;
    }

    public void setAccountnumber(int accountnumber) {
        //
        this.accountnumber = accountnumber;
    }
}
