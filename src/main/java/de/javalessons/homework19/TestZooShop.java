package de.javalessons.homework19;

public class TestZooShop {
    public static void main(String[] args) {
        ZooShop shop1 = new ZooShop("Пушистые лапки", "ул. Хомячков 147-2А", "Кролик", "Питон");
        ZooShop shop2 = new ZooShop("Зверьки и компания", "проспект Пушистика 185");
        shop1.addAnimals("Котенок", "Котенок", "Щенок");
        shop1.addAnimal("Попугай");
        shop2.addAnimal("Хомяк");
        shop2.addAnimal("Хомяк");
        shop2.addAnimal("Кролик");
        shop2.addAnimal("Попугай");
        shop1.listAvailableAnimals();
        shop2.listAvailableAnimals();
        shop1.sellAnimal("Котенок");
        shop2.sellAnimal("Попугай");

        shop1.sellAnimal("Лошадь Пржевальского");
        shop2.sellAnimal("Комары малярийные заразные");

        shop1.listAvailableAnimals();
        shop2.listAvailableAnimals();
    }
}
