package de.javalessons.homework19;

import java.util.ArrayList;
import java.util.List;

public class ZooShop {
    /*
    Создайте класс "Зоомагазин"
    Поля: название, адрес, список доступных для продажи животных.
    Методы: конструктор, sellAnimal(String animalName) - продает животное (удаляет из списка),
    addAnimal(String animalName) - добавляет животное в список.
    Создайте класса "Тест" В этом классе создайте несколько объектов вышеуказанного класса
     и вызовите их методы.
     */
    private String name;
    private String address;
    private List<String> availableAnimals;


    public ZooShop(String name, String address) {
        this.name = name;
        this.address = address;
        this.availableAnimals = new ArrayList<>();
    }

    public ZooShop(String name, String address, String...animalNames) {
        this.name = name;
        this.address = address;
        this.availableAnimals = new ArrayList<>();
        // добавляем животных в магазин
        addAnimals(animalNames);
    }

    public void sellAnimal(String animalName) {
        if (availableAnimals.contains(animalName)) {
            availableAnimals.remove(animalName);
            System.out.println("Из магазина '" + name + "(" + address + ")' продано животное: " + animalName);
        } else {
            System.out.println("Животное '" + animalName + "' не найдено в магазине '" + name + "(" + address + ")'");
        }
        System.out.println("-----------------------");
    }

    public void addAnimal(String animalName) {
        if (availableAnimals.contains(animalName)) {
            System.out.println("Животное '" + animalName + "' уже есть в магазине '" + name + "(" + address + ")'");
            System.out.println("Выберите другое имя, например> " + animalName + " вислоухий рыжий <");
        } else {
            availableAnimals.add(animalName);
            System.out.println("Добавлено животное в магазине '" + name + "(" + address + ")': " + animalName);
        }
        System.out.println("-----------------------");
    }
    public void addAnimals(String...animalNames) {
        for (String name : animalNames) {
            addAnimal(name);
        }
    }
    public void listAvailableAnimals() {
        System.out.println("Список доступных животных в зоомагазине '" + name + "(" + address + ")':");
        for (String animal : availableAnimals) {
            System.out.println(animal);
        }
        System.out.println("-----------------------");
    }




}
