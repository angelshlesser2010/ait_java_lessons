package de.javalessons.homework51;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class FileTextWorker {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileTextWorker.class);


    private static final String SOURCE_FILE = "source.txt";
    private static final String DESTINATION_FILE = "destination.txt";


    public static void main(String[] args) {
        try (FileReader reader = new FileReader(SOURCE_FILE);
             FileWriter writer = new FileWriter(DESTINATION_FILE)) {
            writeReversedTextToFile(reader, writer);
        } catch (FileNotFoundException exception) {
            LOGGER.error("Файл не найден {}", exception.getMessage());

        } catch (IOException exception) {
            LOGGER.error("Ошибка в обработке файлов {}", exception.getMessage());
        }
    }

    public static void writeReversedTextToFile(Reader reader, Writer writer) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(reader);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            String[] words = line.split("\\s+");
            for (int i = 0; i < words.length; i++) {
                String reversedWord = reverseString(words[i]);
                bufferedWriter.write(reversedWord);
                bufferedWriter.write(" ");
            }
            bufferedWriter.newLine();
        }
        bufferedWriter.flush();
        LOGGER.info("Запись успешно завершена");
    }

    public static String reverseString(String wordToReverse) {
        char[] lettersArray = new char[wordToReverse.length()];
        for (int i = 0, j = wordToReverse.length() - 1; i <= j; i++, j--) {
            lettersArray[i] = wordToReverse.charAt(j);
            lettersArray[j] = wordToReverse.charAt(i);
        }
        String wordToReturn = new String(lettersArray);
        LOGGER.info("слово {} успешно обработано. Результат {}", wordToReverse, wordToReturn);
        return wordToReturn;
    }

    public static String reverseStringSmall(String wordToReverse){
        String wordToReturn = new StringBuilder(wordToReverse).reverse().toString();
        LOGGER.info("слово {} успешно обработано. Результат {}", wordToReverse, wordToReturn);
        return wordToReturn;
    }


}
