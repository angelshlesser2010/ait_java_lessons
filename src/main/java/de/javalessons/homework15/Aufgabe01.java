package de.javalessons.homework15;

public class Aufgabe01 {
    /*
     * Напишите метод, который принимает
     * два целых числа и возвращает их сумму.
     */
    public static void main(String[] args) {
        System.out.println(sum(10, 15));
        sumVoid(10, 30);
    }

    static int sum(int numberOne, int numberTwo){
        return numberOne + numberTwo;
    }

    static void sumVoid(int numberOne, int numberTwo){
        int result = numberOne + numberTwo;
        System.out.println(result);
    }

}
