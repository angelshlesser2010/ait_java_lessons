package de.javalessons.homework52;

import de.javalessons.homework51.FileTextWorker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class AirportFileReader {

    private static final Logger LOGGER = LoggerFactory.getLogger(AirportFileReader.class);


    //private static  String path = "airports.txt";
    private static  String path = "airportsWorld.txt";

    public static void main(String[] args) {

        try {
            FileReader fileReader = new FileReader(path);

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line;

            while ((line = bufferedReader.readLine()) != null){
                String [] parts = line.split(";", 3);
                if(parts.length == 3){
                    //название
                    String name = parts[0];
                    //IATA код
                    String iata = parts[1];
                    //название страны
                    String country = parts [2];
                    LOGGER.info("Название: {} Код IATA: {} Страна: {}", name, iata, country);
                    System.out.println("Название: " + name + " Код IATA: " + iata + " Страна: " + country);
                }
            }
            bufferedReader.close();

        }
        catch (FileNotFoundException exception){
            LOGGER.error("Файл  {} не был найден {}", path, exception.getMessage());
        } catch (IOException exception) {
            LOGGER.error("Файл  {} невозможно считать {}", path, exception.getMessage());
        }

    }
}
